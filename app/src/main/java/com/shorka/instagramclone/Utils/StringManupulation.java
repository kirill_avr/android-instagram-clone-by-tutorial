package com.shorka.instagramclone.Utils;

/**
 * Created by Kyrylo Avramenko on 4/13/2018.
 */
public class StringManupulation {

    public static String expandUsername(String username){
        return username.replace("."," ");
    }

    public static String condenseUsername(String username){
        return username.replace(" ",".");
    }
}
