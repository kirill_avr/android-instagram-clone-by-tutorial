package com.shorka.instagramclone.Utils;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.util.Log;
import android.view.MenuItem;

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.shorka.instagramclone.Home.HomeActivity;
import com.shorka.instagramclone.Likes.LikesActivity;
import com.shorka.instagramclone.Profile.ProfileActivity;
import com.shorka.instagramclone.R;
import com.shorka.instagramclone.Search.SearchActivity;
import com.shorka.instagramclone.Share.ShareActivity;

/**
 * Created by Kyrylo Avramenko on 4/6/2018.
 */
public class BottomNavigationViewHelper {

    private static final String TAG = "BottomNavigationViewHel";

    public static void setBottomNavigationView(BottomNavigationViewEx bottomNavigationViewEx){
        Log.d(TAG, "setupBottomNavigationView: Setting up BottomNavigationView");
        bottomNavigationViewEx.enableAnimation(false);
        bottomNavigationViewEx.enableItemShiftingMode(false);
        bottomNavigationViewEx.enableShiftingMode(false);
        bottomNavigationViewEx.setTextVisibility(false);
    }

    public static void enableNavigation(final Context context, BottomNavigationViewEx view){
        view.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
                 @Override
                 public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                     Intent intent = null;
                     switch (item.getItemId()){
                         case R.id.ic_house:
                             intent = new Intent(context, HomeActivity.class);
                            break;

                         case R.id.ic_search:
                             intent = new Intent(context, SearchActivity.class);
                             break;

                         case R.id.ic_circle:
                             intent = new Intent(context, ShareActivity.class);
                             break;

                         case R.id.ic_alert:
                             intent = new Intent(context, LikesActivity.class);
                             break;

                         case R.id.ic_profile:
                             intent = new Intent(context, ProfileActivity.class);
                             break;
                     }
                     if(intent !=null){
                         context.startActivity(intent);
                     }
//                     else
//                         Log.e(TAG, "onNavigationItemSelected: intent is NULL");

                     return false;
                 }
             }
        );

    }
}
