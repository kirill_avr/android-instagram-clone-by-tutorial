package com.shorka.instagramclone.Profile;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shorka.instagramclone.R;

/**
 * Created by Kyrylo Avramenko on 4/7/2018.
 */
public class SignOutFragment extends android.support.v4.app.Fragment {
    private static final String TAG = "EditProfileFragment";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_signout, container, false);
        return view;
    }
}
